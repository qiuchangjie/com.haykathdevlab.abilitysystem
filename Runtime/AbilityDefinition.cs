using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime
{
    [CreateAssetMenu(fileName = "New Ability", menuName = "Ability Definition", order = 0)]
    public class AbilityDefinition : SerializedScriptableObject
    {
        public string displayName = "New Ability";
        
        [TextArea]
        public string description = "A new ability.";
        
        public float cooldown = 1f;
        
        public bool isChanneled;
        
        [ShowIf("isChanneled")]
        public float channelTickTime = 0.33f;
        [ShowIf("isChanneled"), Tooltip("The maximum time for which the skill can be held. Set to 0 or less to disable.")]
        public float maxChannelTime = 1f;
        
        [ShowIf("isChanneled")]
        public bool applyCostsOnChannelTick;
        
        [ShowIf("isChanneled")]
        public bool dontApplyFirstChannelTick;

        public List<IAbilityCost> costs = new List<IAbilityCost>();
        
        public List<IAbilityAction> onBeforeCast = new List<IAbilityAction>();
        public List<IAbilityAction> onCast = new List<IAbilityAction>();
        public List<IAbilityAction> onAfterCast = new List<IAbilityAction>();
        
        public List<IAbilityAction> onProjectileHit = new List<IAbilityAction>();

        public AbilityData ToAbilityData() {
            var data = new AbilityData
            {
                Cooldown = cooldown,
                IsChanneled = isChanneled,
                ChannelTickTime = channelTickTime,
                MaxChannelTime = maxChannelTime,
                ApplyCostsOnChannelTick = applyCostsOnChannelTick,
                DontApplyFirstChannelTick = dontApplyFirstChannelTick,
                
                Costs = costs,
                
                OnBeforeCast = onBeforeCast,
                OnCast = onCast,
                OnAfterCast = onAfterCast,
                
                OnProjectileHit = onProjectileHit
            };

            return data;
        }

        public Ability ToAbility() {
            return new Ability(ToAbilityData());
        }
        
        public Ability ToAbility(Blackboard sharedBlackboard) {
            return new Ability(ToAbilityData(), sharedBlackboard);
        }
    }
}