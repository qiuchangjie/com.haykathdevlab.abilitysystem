using System;
using System.Collections;
using HaykathDevLab.AbilitySystem.Runtime.Utils;
using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime.Actions
{
    [Serializable]
    public class FireProjectile : IAbilityAction
    {
        public GameObject projectilePrefab;
        public float speed;
        public float maxLifetime = 5f;
        
        public IEnumerator Execute(Ability owner, Vector3 castPoint, Vector3 targetPoint, GameObject targetObj) {
           
            var dir = (targetPoint - castPoint).normalized;
            var rotation = Quaternion.LookRotation(dir, Vector3.up);
            
            var instance = ProjectilePoolManager.Pool.GetInstance(projectilePrefab, castPoint, rotation);

            if (!instance.TryGetComponent<IProjectile>(out var projectile))
            {
                Debug.LogError("Projectile prefab does not have a projectile component!");
                yield return null;
            }

            projectile.OnProjectileHit += owner.OnProjectileHit;
            projectile.Velocity = dir * speed;
            
            ProjectilePoolManager.Pool.Dispose(instance, maxLifetime);
            
            yield return null;
        }
    }
}