using System;
using System.Collections;
using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime.Actions
{
    [Serializable]
    public class PlayAnimatorState : IAbilityAction
    {
        public string stateName = "State";
        public int layer = 0;
        
        public bool waitForAnimationEnd;
        
        public IEnumerator Execute(Ability owner, Vector3 castPoint, Vector3 targetPoint, GameObject targetObj) {
            var casterObj = owner.Caster.GameObject;
            if (!casterObj.TryGetComponent<Animator>(out var animator))
            {
                Debug.LogError("Caster has no animator!", casterObj);
                yield break;
            }
            
            animator.Play(stateName, layer);
            
            if (waitForAnimationEnd)
            {
                yield return null;
                var stateInfo = animator.GetCurrentAnimatorStateInfo(layer);
                var animationTime = stateInfo.length;
                Debug.Log(animationTime);
                yield return new WaitForSeconds(animationTime);
            }

            yield return null;
        }
    }
}