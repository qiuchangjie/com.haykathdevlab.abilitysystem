﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime
{
    public class Ability
    {       
        private readonly float _channelTickTime;
        private readonly float _maxChannelTime;
        private readonly bool _applyCostsOnChannelTick;
        private readonly bool _dontApplyFirstChannelTick;

        private readonly IAbilityCost[] _costs;
        
        private readonly IAbilityAction[] _onBeforeCast;
        private readonly IAbilityAction[] _onCast;
        private readonly IAbilityAction[] _onAfterCast;
        private readonly IAbilityAction[] _onProjectileHit;

        private float _lastExecutionTime;

        private bool _shouldBeInterrupted;
        
        public Blackboard Blackboard { get; } = new Blackboard();

        public float Cooldown { get; }

        public bool IsChanneled { get; }

        public bool IsExecuting { get; private set; }

        public IAbilityUser Caster { get; private set; }

        public bool IsCastable => !IsExecuting && Time.time >= _lastExecutionTime + Cooldown && CheckCosts();

        public Ability(AbilityData data) {
            Cooldown = data.Cooldown;
            IsChanneled = data.IsChanneled;
            _channelTickTime = data.ChannelTickTime;
            _maxChannelTime = data.MaxChannelTime;
            _applyCostsOnChannelTick = data.ApplyCostsOnChannelTick;
            _dontApplyFirstChannelTick = data.DontApplyFirstChannelTick;
            
            _costs = data.Costs != null ? data.Costs.ToArray() : new IAbilityCost[0];

            _onBeforeCast = data.OnBeforeCast != null ? data.OnBeforeCast.ToArray() : new IAbilityAction[0];
            _onCast = data.OnCast != null ? data.OnCast.ToArray() : new IAbilityAction[0];
            _onAfterCast = data.OnAfterCast != null ? data.OnAfterCast.ToArray() : new IAbilityAction[0];
            _onProjectileHit = data.OnProjectileHit != null ? data.OnProjectileHit.ToArray() : new IAbilityAction[0];

            _lastExecutionTime = float.MinValue;
        }

        public Ability(AbilityData data, Blackboard blackboard) : this(data) {
            Blackboard = blackboard ?? throw new Exception("Cannot create ability with null blackboard!");
        }

        public IEnumerator Execute(IAbilityUser caster) {
            if(IsExecuting) yield break;
            
            IsExecuting = true;
            _shouldBeInterrupted = false;

            //Setup
            Caster = caster;

            //Cooldown
            if(Time.time < _lastExecutionTime + Cooldown)
            {
                Debug.LogError("Skill cooldown is not ready.");
                IsExecuting = false;
                yield break; //Cooldown not met
            }

            if (!CheckCosts())
            {
                Debug.LogError("Skill costs not met.");
                IsExecuting = false;
                yield break; //Costs not met
            }

            if (!_applyCostsOnChannelTick)
            {
                ApplyCosts();
            }

            yield return ExecuteActions(_onBeforeCast, Caster.TargetPoint, Caster.TargetObject);

            yield return StartExecution();                
            yield return End();
        }

        public void Interrupt()
        {
            _shouldBeInterrupted = true;
        }

        public IEnumerator OnProjectileHit(Collision collisionData) {
            yield return ExecuteActions(_onProjectileHit, collisionData.contacts[0].point, collisionData.gameObject, false);
        }

        private IEnumerator StartExecution() {
            _lastExecutionTime = Time.time;

            if (IsChanneled)
            {
                var channelTickTimer = new WaitForSeconds(_channelTickTime);
                var channelEndTime = _maxChannelTime > 0 ? _lastExecutionTime + _maxChannelTime : float.MaxValue; //Ignore max channel time if it is 0 or less

                if (_dontApplyFirstChannelTick)
                {
                    yield return channelTickTimer;
                }

                while (!_shouldBeInterrupted && Time.time < channelEndTime)
                {
                    if (_applyCostsOnChannelTick)
                    {
                        if (!CheckCosts())
                        {
                            Debug.LogError("Skill costs not met.");
                            yield break; //Costs not met
                        }

                        ApplyCosts();
                    }
                    yield return ExecuteActions(_onCast, Caster.TargetPoint, Caster.TargetObject);
                    yield return channelTickTimer;
                }
            }
            else
            {
                yield return ExecuteActions(_onCast, Caster.TargetPoint, Caster.TargetObject);
            }
        }
        
        private IEnumerator End() {
            IsExecuting = false;
            _shouldBeInterrupted = false;
            
            yield return ExecuteActions(_onAfterCast, Caster.TargetPoint, Caster.TargetObject);
        }

        private bool CheckCosts() {
            for (var i = 0; i < _costs.Length; i++)
            {
                if (!_costs[i].Check(Caster))
                {
                    return false;
                }
            }

            return true;
        }

        private void ApplyCosts() {
            foreach (var cost in _costs)
            {
                cost.Apply(Caster);
            }
        }

        private IEnumerator ExecuteActions(IAbilityAction[] actions, Vector3 targetPoint, GameObject targetObj, bool interruptable = true) {
            for (var i = 0; i < actions.Length; i++)
            {
                if (interruptable && _shouldBeInterrupted)
                {
                    yield break;
                }
                var action = actions[i];
                yield return action.Execute(this, Caster.CastPoint, targetPoint, targetObj);
            }
        }
    }
}
