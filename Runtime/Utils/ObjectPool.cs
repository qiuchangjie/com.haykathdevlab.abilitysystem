using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace HaykathDevLab.AbilitySystem.Runtime.Utils
{
    public class ObjectPool
    {
        private readonly MonoBehaviour _manager;
        private readonly Dictionary<GameObject, List<PooledObject>> _objects;

        public ObjectPool(MonoBehaviour manager) {
            _manager = manager;
            _objects = new Dictionary<GameObject, List<PooledObject>>();
        }

        public GameObject GetInstance(GameObject prefab) {
            PooledObject poolObj = null;
            GameObject instance;
            var containsPrefabKey = _objects.ContainsKey(prefab);
            
            if (containsPrefabKey)
            {
                poolObj = _objects[prefab].Find(x => !x.isActive);    
            }

            if (poolObj == null)
            {
                poolObj = GetNewInstance(prefab);
                instance = poolObj.gameObject;

                if (!containsPrefabKey)
                {
                    _objects.Add(prefab, new List<PooledObject>());
                }
                
                _objects[prefab].Add(poolObj);
            }
            else
            {
                instance = poolObj.gameObject;
                poolObj.generation++;
            }

            poolObj.isActive = true;
            instance.transform.SetParent(null);
            instance.SetActive(true);
            return instance;
        }

        public GameObject GetInstance(GameObject prefab, Vector3 position, Quaternion rotation) {
            var inst = GetInstance(prefab);
            inst.transform.position = position;
            inst.transform.rotation = rotation;

            return inst;
        }

        public void Dispose(GameObject obj) {
            if(!obj.TryGetComponent<PooledObject>(out var poolObj))
                throw new Exception($"{obj} is not a pooled object.");

            if (poolObj.pool != this)
            {
                throw new Exception($"{obj} does not belong to this pool.");
            }

            if (poolObj.isActive == false)
            {
                return;
            }

            poolObj.isActive = false;
            poolObj.gameObject.SetActive(false);
            poolObj.transform.SetParent(_manager.transform);
        }

        public void Dispose(GameObject obj, float delay) {
            if(!obj.TryGetComponent<PooledObject>(out var poolObj))
                throw new Exception($"{obj} is not a pooled object.");

            if (poolObj.pool != this)
            {
                throw new Exception($"{obj} does not belong to this pool.");
            }
            
            if (poolObj.isActive == false)
            {
                return;
            }
            
            _manager.StartCoroutine(DelayedDestroy(poolObj, delay, poolObj.generation));
        }

        private PooledObject GetNewInstance(GameObject prefab) {
            var instance = Object.Instantiate(prefab);
            var poolObj = instance.AddComponent<PooledObject>();
            poolObj.prefab = prefab;
            poolObj.pool = this;
            poolObj.generation = 0;

            return poolObj;
        }

        private IEnumerator DelayedDestroy(PooledObject poolObj, float seconds, int generation) {
            yield return new WaitForSeconds(seconds);
            if (generation != poolObj.generation) yield break;
            poolObj.isActive = false;
            poolObj.gameObject.SetActive(false);
            poolObj.transform.SetParent(_manager.transform);
        }
        
        public class PooledObject : MonoBehaviour
        {
            public ObjectPool pool;
            public GameObject prefab;
            public bool isActive;
            public int generation;
        }
    }
}