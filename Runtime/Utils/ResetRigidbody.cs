using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime.Utils
{
    [RequireComponent(typeof(Rigidbody))]
    public class ResetRigidbody : MonoBehaviour
    {
        private Rigidbody _rb;

        private void Awake() {
            _rb = GetComponent<Rigidbody>();
        }

        private void OnDisable() {
            _rb.velocity = Vector3.zero;
            _rb.angularVelocity = Vector3.zero;
        }
    }
}