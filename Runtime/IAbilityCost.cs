namespace HaykathDevLab.AbilitySystem.Runtime
{
    public interface IAbilityCost
    {
        bool Check(IAbilityUser caster);

        void Apply(IAbilityUser caster);
    }
}