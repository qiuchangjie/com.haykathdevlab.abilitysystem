using HaykathDevLab.AbilitySystem.Runtime;

namespace HaykathDevLab.AbilitySystem.Tests
{
    public class ResourceCost : IAbilityCost
    {
        private ResourceHandler _resourceHandler;
        private int _cost;
        
        public ResourceCost(ResourceHandler handler, int cost) {
            _resourceHandler = handler;
            _cost = cost;
        }
        
        public bool Check(IAbilityUser caster) {
            return _resourceHandler.ResourceCount >= _cost;
        }

        public void Apply(IAbilityUser caster) {
            _resourceHandler.Take(_cost);
        }
    }
}