﻿using System.Collections;
using HaykathDevLab.AbilitySystem.Runtime;
using HaykathDevLab.AbilitySystem.Tests.Builders;
using HaykathDevLab.AbilitySystem.Tests.Components;
using UnityEngine;
using UnityEngine.TestTools;

namespace HaykathDevLab.AbilitySystem.Tests
{
    public class AbilityTests
    {
        private class StubCaster : IAbilityUser
        {
            public GameObject GameObject { get; } = null;
            public Vector3 CastPoint { get; } = Vector3.zero;
            public Vector3 TargetPoint { get; } = Vector3.forward;
            public GameObject TargetObject { get; } = null;
        }
        
        [UnityTest]
        public IEnumerator EmptyAbility()
        {
            Ability ability = AbilityBuilder.Create;
            
            yield return ability.Execute(new StubCaster());
        }
        
        [UnityTest]
        public IEnumerator OnCastMessage() {
            const string message = "Cast Action";
            
            Ability ability = AbilityBuilder.Create.WithCastAction(ActionBuilder.Message(message));
            
            LogAssert.Expect(LogType.Log, message);
            yield return ability.Execute(new StubCaster());
        }
        
        [UnityTest]
        public IEnumerator OnBeforeCastMessage() {
            const string message = "Before Cast Action";
            
            Ability ability = AbilityBuilder.Create.WithBeforeCastAction(ActionBuilder.Message(message));
            
            LogAssert.Expect(LogType.Log, message);
            yield return ability.Execute(new StubCaster());
        }
        
        [UnityTest]
        public IEnumerator OnAfterCastMessage() {
            const string message = "After Cast Action";
            
            Ability ability = AbilityBuilder.Create.WithAfterCastAction(ActionBuilder.Message(message));
            
            LogAssert.Expect(LogType.Log, message);
            yield return ability.Execute(new StubCaster());
        }

        [UnityTest]
        public IEnumerator DoubleCastMessage()
        {
            const string m1 = "First";
            const string m2 = "Second";

            Ability ability = AbilityBuilder.Create
                .WithCastAction(ActionBuilder.Message(m1))
                .WithCastAction(ActionBuilder.Message(m2));

            LogAssert.Expect(LogType.Log, m1);
            LogAssert.Expect(LogType.Log, m2);

            yield return ability.Execute(new StubCaster());
        }

        [UnityTest]
        public IEnumerator Cooldown()
        {
            Ability ability = AbilityBuilder.Create.WithCooldown(2f);

            LogAssert.Expect(LogType.Error, "Skill cooldown is not ready.");

            yield return ability.Execute(new StubCaster()); //First cast, should be ok
            yield return ability.Execute(new StubCaster()); //Should fail due to cooldown time
        }

        [UnityTest]
        public IEnumerator ChanneledAbilityInterrupt()
        {
            yield return new MonoBehaviourTest<ChanneledAbilityTest>();
            
            LogAssert.NoUnexpectedReceived();
        }
        
        [UnityTest]
        public IEnumerator ChanneledAbilityEnd()
        {
            const string castMessage = "Channel tick";
            
            Ability ability = AbilityBuilder.Create
                .Channeled()
                .WithChannelTickTime(.3f)
                .WithMaxChannelTime(1f)
                .WithCastAction(ActionBuilder.Message(castMessage))
                .DontApplyFirstChannelTick();
            
            LogAssert.Expect(LogType.Log, castMessage);
            LogAssert.Expect(LogType.Log, castMessage);
            LogAssert.Expect(LogType.Log, castMessage);

            yield return ability.Execute(new StubCaster());
            
            LogAssert.NoUnexpectedReceived();
        }

        [UnityTest]
        public IEnumerator ResourceCost() {
            const string message = "Cost paid";
            
            var rm = new ResourceHandler(1);

            Ability ability = AbilityBuilder.Create
                .WithCost(CostBuilder.ResourceCost(rm, 1))
                .WithCastAction(ActionBuilder.Message(message));

            LogAssert.Expect(LogType.Log, message);
            
            yield return ability.Execute(new StubCaster());
        }
        
        [UnityTest]
        public IEnumerator ResourceCostFail() {
            var rm = new ResourceHandler(1);

            Ability ability = AbilityBuilder.Create
                .WithCost(CostBuilder.ResourceCost(rm, 2));

            LogAssert.Expect(LogType.Error, "Skill costs not met.");
            
            yield return ability.Execute(new StubCaster());
        }
        
        [UnityTest]
        public IEnumerator RepeatingResourceCost() {

            var rm = new ResourceHandler(3);

            Ability ability = AbilityBuilder.Create
                .WithCost(CostBuilder.ResourceCost(rm, 1))
                .Channeled()
                .WithChannelTickTime(.3f)
                .WithMaxChannelTime(1f)
                .ApplyCostsOnChannelTick()
                .DontApplyFirstChannelTick();

            yield return ability.Execute(new StubCaster());
            
            LogAssert.NoUnexpectedReceived();
        }
        
        [UnityTest]
        public IEnumerator RepeatingResourceCostMidFail() {
            var rm = new ResourceHandler(2);

            Ability ability = AbilityBuilder.Create
                .WithCost(CostBuilder.ResourceCost(rm, 1))
                .Channeled()
                .WithChannelTickTime(.3f)
                .WithMaxChannelTime(1f)
                .ApplyCostsOnChannelTick();
            
            LogAssert.Expect(LogType.Error, "Skill costs not met.");
            
            yield return ability.Execute(new StubCaster());
            
            LogAssert.NoUnexpectedReceived();
        }
    }
}

