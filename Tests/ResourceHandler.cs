namespace HaykathDevLab.AbilitySystem.Tests
{
    public class ResourceHandler
    {
        public int ResourceCount { get; private set; }

        public ResourceHandler(int initialResourceCount = 5) {
            ResourceCount = initialResourceCount;
        }

        public bool Take(int amount) {
            if (amount < 0 || ResourceCount < amount)
            {
                return false;
            }

            ResourceCount -= amount;
            return true;
        }
    }
}